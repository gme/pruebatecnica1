#!/bin/python


"""

Toolbox Argentina Excercise 1

Descripcion de Ejercicio:

Escribe un script que a partir de un array de ints
devuelva un array de strings aplicando las siguientes reglas:
    - Devuelve Fizz si el número es divisible por 3 o si incluye un 3 en el número.
    - Devuelve Buzz si el número es divisible por 5 o si incluye un 5 en el número.
    - Devuelve FizzBuzz si el número es divisible por 3 y por 5.
Puedes utilizar cualquier lenguaje que consideres apropiado.


"""
import sys



a=map(str,sys.argv[1:])
output = []
for x in a:
    if int(x) % 3 == 0 and int(x) % 5 == 0:
        output.append("FizzBuzz")
        continue
    if int(x) % 5 == 0 or "5" in x:
        output.append("BUZZ")
        continue
    if int(x) % 3 == 0 or "3" in x:
        output.append("Fizz")
        continue


#Devuelve un array de strings con el resultado de cada condicion
print (output)
